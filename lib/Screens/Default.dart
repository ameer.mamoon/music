// ignore_for_file: file_names, use_key_in_widget_constructors

import 'package:flutter/material.dart';


class DefaultScreen extends StatelessWidget {
  final Widget body;
  final Drawer? drawer;
  final List<Widget>? actions;
  final String title;


  DefaultScreen({required this.body, this.drawer, this.actions,required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(title),
        centerTitle: true,
        actions: actions,
    ),
    );
  }
}
